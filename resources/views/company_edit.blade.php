<x-app-layout>
    <x-slot name="header">
        <div class="container-xl">
            <!-- Page title -->
            <div class="page-header d-print-none">
                <div class="row align-items-center">
                    <div class="col">
                        <!-- Page pre-title -->
                        <div class="page-pretitle">
                            Edit
                        </div>
                        <h2 class="page-title">
                            Company
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>

    <div class="container-xl">
        <div class="row">
            <div class="col-8">
                <form action="{{ route('updateCompany', ['company' => $company->id ]) }}" method="post">
                    @method('PUT')
                    @csrf
                    {{-- <input type="hidden" name="_company" value="{{ $company->id }}"> --}}
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Edit Company</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="mb-3">
                                        <label class="form-label">Company Name</label>
                                        <input type="text" class="form-control" name="name"
                                            placeholder="Your company name" value="{{ $company->name }}">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="mb-3">
                                        <label class="form-label">Industry</label>
                                        <select class="form-select" name="type">
                                            <option {{ $company->type === 'construction' ? 'selected="selected"' : '' }} value="construction" selected>Construction</option>
                                            <option {{ $company->type === 'service' ? 'selected="selected"' : '' }} value="service">Service Sector</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label class="form-label">Registration Number</label>
                                        <input type="text" class="form-control" name="regi_no" value="{{ $company->regi_no }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label class="form-label">Renewal Date</label>
                                        <input type="date" class="form-control" name="renewal_date" value="{{ $company->renewal_date }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div>
                                        <label class="form-label">Address</label>
                                        <textarea class="form-control" rows="3" name="address">
                                            {{ html_entity_decode($company->address) }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-auto">
                                    <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                                        Cancel
                                    </a>
                                </div>
                                <div class="col-auto ms-auto">
                                    <button type="submit" class="btn btn-primary ms-auto">
                                        
                                        Update
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <x-slot name="modal">

    </x-slot>
</x-app-layout>
