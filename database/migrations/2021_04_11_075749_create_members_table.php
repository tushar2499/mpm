<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained('companies');
            $table->string('member_id')->unique();
            $table->string('passport_no');
            $table->string('passport_surname');
            $table->string('passport_givenname');
            $table->string('group_name');
            $table->date('passport_expiry');
            $table->date('passport_issue');
            $table->date('visa_expiry');
            $table->string('visa_status');
            $table->date('visa_issue');
            $table->string('medical_status');
            $table->date('medial_date');
            $table->date('birth_date');
            $table->integer('mobile_my');
            $table->integer('mobile_bd');
            $table->integer('mobile_emergency');
            $table->string('email');
            $table->string('photo');
            $table->text('present_address');
            $table->text('permanent_address');
            $table->string('bank_letter');
            $table->string('Memo Country');
            $table->string('out_station');
            $table->string('current_status');
            $table->string('special_note');
            $table->string('file_close');
            $table->string('passport_status');
            $table->date('cidb_submission_date');
            $table->date('cidb_delivery_date');
            $table->string('cidb_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
