<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\MemberController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->group(function() {
    
    Route::get('/dashboard', function() {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/company', [CompanyController::class, 'index'])->name('company');
    Route::post('/company/save', [CompanyController::class, 'save']);
    Route::delete('/company/delete/{company}', [CompanyController::class, 'delete'])->name('companyDelete');
    Route::get('/company/edit/{company}', [CompanyController::class, 'edit'])->name('companyEdit');
    Route::put('/company/update/{company}', [CompanyController::class, 'update'])->name('updateCompany');

    Route::get('/member', [MemberController::class, 'index'])->name('member');

});

require __DIR__.'/auth.php';
