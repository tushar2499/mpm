<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;

class CompanyController extends Controller
{

    public function index() {
        return view('company', ['companies' => Company::all()] );
    }

    public function edit( Request $request, Company $company ) {
        return view('company_edit', ['company' => $company ] );
    }

    public function save( Request $request ) {
        $company = Company::create($request->all() );

        return redirect('company')->with('status', 'Company created!');
    }

    public function update( Request $request, Company $company ) {
        
        $company->fill( $request->all() )->save();

        return redirect('company')->with('status', 'Company updated!');
    }

    public function delete( Request $request, Company $company ) {
        
        $company->delete();

        return redirect('company')->with('status', 'Company deleted!');

    }
}
